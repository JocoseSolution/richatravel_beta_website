﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForPack.master" AutoEventWireup="true" CodeFile="Package_List.aspx.cs" Inherits="Package_Package_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="theme-page-section theme-page-section-dark">
        <div class="container">
            <div class="theme-search-area _mob-h theme-search-area-white">
                <div class="row">
                    <div class="col-md-3 ">
                        <div class="theme-search-area-header theme-search-area-header-sm">

                            <h1 class="theme-search-area-title">
                                <asp:Label ID="Total" runat="server" Text=""></asp:Label>
                                Package Found </h1>
                            <p class="theme-search-area-subtitle">June 27 &rarr; July 02, 2 Guests</p>
                        </div>
                    </div>
<%--                    <div class="col-md-9 ">
                        <div class="theme-search-area-form" id="hero-search-form">
                            <div class="row" data-gutter="10">
                                <div class="col-md-4 ">
                                    <div class="theme-search-area-section first theme-search-area-section-fade-white theme-search-area-section-sm theme-search-area-section-no-border theme-search-area-section-curved">
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                                            <input class="theme-search-area-section-input typeahead" value="London" type="text" placeholder="Destination" data-provide="typeahead" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7 ">
                                    <div class="row" data-gutter="10">
                                        <div class="col-md-4 ">
                                            <div class="theme-search-area-section theme-search-area-section-fade-white theme-search-area-section-sm theme-search-area-section-no-border theme-search-area-section-curved">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                                    <input class="theme-search-area-section-input datePickerStart _mob-h" value="Wed 06/27" type="text" placeholder="Check-in" />
                                                    <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-06-27" type="date" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 ">
                                            <div class="theme-search-area-section theme-search-area-section-fade-white theme-search-area-section-sm theme-search-area-section-no-border theme-search-area-section-curved">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                                    <input class="theme-search-area-section-input datePickerEnd _mob-h" value="Mon 07/02" type="text" placeholder="Check-out" />
                                                    <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-07-02" type="date" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 ">
                                            <div class="theme-search-area-section theme-search-area-section-fade-white theme-search-area-section-sm theme-search-area-section-no-border theme-search-area-section-curved quantity-selector" data-increment="Guests">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon lin lin-people"></i>
                                                    <input class="theme-search-area-section-input" value="2 Guests" type="text" />
                                                    <div class="quantity-selector-box" id="ExpSearchGuests">
                                                        <div class="quantity-selector-inner">
                                                            <p class="quantity-selector-title">Guests</p>
                                                            <ul class="quantity-selector-controls">
                                                                <li class="quantity-selector-decrement">
                                                                    <a href="#">&#45;</a>
                                                                </li>
                                                                <li class="quantity-selector-current">1</li>
                                                                <li class="quantity-selector-increment">
                                                                    <a href="#">&#43;</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 ">
                                    <button class="theme-search-area-submit _mt-0 _tt-uc theme-search-area-submit-no-border theme-search-area-submit-curved theme-search-area-submit-sm theme-search-area-submit-primary">Edit</button>
                                </div>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>


            <div class="theme-search-area-inline _desk-h theme-search-area-inline-white">
                <h4 class="theme-search-area-inline-title">London Experiences</h4>
                <p class="theme-search-area-inline-details">June 27 &rarr; July 02, 2 Guests</p>
                <a class="theme-search-area-inline-link magnific-inline" href="#searchEditModal">
                    <i class="fa fa-pencil"></i>Edit
          </a>
                <div class="magnific-popup magnific-popup-sm mfp-hide" id="searchEditModal">
                    <div class="theme-search-area theme-search-area-vert">
                        <div class="theme-search-area-header">
                            <h1 class="theme-search-area-title theme-search-area-title-sm">Edit your Search</h1>
                            <p class="theme-search-area-subtitle">Prices might be different from current results</p>
                        </div>
                        <div class="theme-search-area-form">
                            <div class="theme-search-area-section first theme-search-area-section-curved">
                                <label class="theme-search-area-section-label">Where</label>
                                <div class="theme-search-area-section-inner">
                                    <i class="theme-search-area-section-icon lin lin-location-pin"></i>
                                    <input class="theme-search-area-section-input typeahead" value="New York" type="text" placeholder="Destination" data-provide="typeahead" />
                                </div>
                            </div>
                            <div class="row" data-gutter="10">
                                <div class="col-md-6 ">
                                    <div class="theme-search-area-section theme-search-area-section-curved">
                                        <label class="theme-search-area-section-label">From</label>
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                            <input class="theme-search-area-section-input datePickerStart _mob-h" value="Wed 06/27" type="text" placeholder="Check-in" />
                                            <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-06-27" type="date" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="theme-search-area-section theme-search-area-section-curved">
                                        <label class="theme-search-area-section-label">To</label>
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon lin lin-calendar"></i>
                                            <input class="theme-search-area-section-input datePickerEnd _mob-h" value="Mon 07/02" type="text" placeholder="Check-out" />
                                            <input class="theme-search-area-section-input _desk-h mobile-picker" value="2018-07-02" type="date" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-gutter="10">
                                <div class="col-md-6 ">
                                    <div class="theme-search-area-section theme-search-area-section-curved quantity-selector" data-increment="Guests">
                                        <label class="theme-search-area-section-label">Guests</label>
                                        <div class="theme-search-area-section-inner">
                                            <i class="theme-search-area-section-icon lin lin-people"></i>
                                            <input class="theme-search-area-section-input" value="2 Guests" type="text" />
                                            <div class="quantity-selector-box" id="mobile-ExpSearchGuests">
                                                <div class="quantity-selector-inner">
                                                    <p class="quantity-selector-title">Guests</p>
                                                    <ul class="quantity-selector-controls">
                                                        <li class="quantity-selector-decrement">
                                                            <a href="#">&#45;</a>
                                                        </li>
                                                        <li class="quantity-selector-current">1</li>
                                                        <li class="quantity-selector-increment">
                                                            <a href="#">&#43;</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" data-gutter="10">
                                <div class="col-md-6 ">
                                    <button class="theme-search-area-submit _mt-0 _tt-uc theme-search-area-submit-curved">Change</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="theme-page-section theme-page-section-gray">
        <div class="container">
            <div class="row row-col-static" id="sticky-parent">

                <div class="col-md-12 ">
                    <div class="theme-search-results-sort-select _desk-h">
                        <select>
                            <option>Price</option>
                            <option>Guest Rating</option>
                            <option>Property Class</option>
                            <option>Property Name</option>
                            <option>Recommended</option>
                            <option>Most Popular</option>
                            <option>Trendy Now</option>
                            <option>Best Deals</option>
                        </select>
                    </div>
                    <div class="theme-search-results _mb-10">
                        <div class="row row-col-gap" data-gutter="10">
                            <asp:Repeater ID="Package" runat="server">
                                <ItemTemplate>

                                    <div class="col-md-3 ">
                                        <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                                            <div class="banner _h-20vh _h-mob-30vh banner-">
                                                <%--                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>--%>
                                                <div class="banner-bg" style='background-image: url(http://admin.Richa Travelstechnology.com/HolidaysImage/<%# Eval("pkg_Image") %>);'></div>

                                            </div>
                                            <div class="theme-search-results-item-grid-body">
                                                <a class="theme-search-results-item-mask-link" href="Package_Details.aspx?Pkg_Id=<%# Eval("pkg_Id") %> & counter=<%# Eval("counter") %>"></a>
                                                <div class="theme-search-results-item-grid-header">
                                                    <h5 class="theme-search-results-item-title"><%# Eval("pkg_Title") %></h5>
                                                </div>
                                                <div class="theme-search-results-item-grid-caption">
                                                    <div class="row" data-gutter="10">
                                                        <div class="col-xs-5 ">
                                                            <div class="theme-search-results-item-rating">
                                                               
                                                                <p class="theme-search-results-item-rating-title"><%# Eval("pkg_noofday") %> Days/<%# Eval("pkg_noofnight") %> Nights</p>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-7 ">
                                                            <div class="theme-search-results-item-price">
<%--                                                                <p class="theme-search-results-item-price-tag">₹ <%# Eval("pkg_price") %></p>--%>
                                               <%#Convert.ToBoolean(Eval("pkg_offerPrice")) == true ? "<div class='d-block text-3 text-black-50 mr-2 mr-lg-3'><del class='d-block'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</del></div><div class='text-dark font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_Offer_price_value")), 2)+"</div>" : "<div class='text-dark  font-weight-500 mr-2 mr-lg-3' style='color: red !important;font-size: 1.1rem !important;'>₹ "+Math.Round(Convert.ToDecimal(Eval("pkg_price")), 2)+"</div>" %>

                                                                <p class="theme-search-results-item-price-sign">person</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                            <%--  <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Join a relaxed bike tour along a beautiful river</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">99 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$197</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Sweat it out with a street dance pro</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">157 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$43</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Drink Tea. Slow Down. Connect. Feel Ordinary Magic</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">15 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$87</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Great live jazz, beautiful setting.</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">124 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$136</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Original Cookie Making Workshop</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">83 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$106</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Blood and Tears: Horror Walking Tour</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">163 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$82</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See London By Night Bus Tour</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">186 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$90</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">London Creative Photography Course</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">123 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$45</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See top London sites whilst learning the ukulele</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">71 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$162</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Make macarons with a top pastry chef</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">35 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$193</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Meet London artists and create modern art</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">169 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$32</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See the city with a social running group</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">159 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$127</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Learn to make sushi and poke with a chef</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">62 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$47</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Enjoy a private show then the magician will divulge his secrets</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">102 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$53</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Go inside local boutiques with a retail insider</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">84 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$200</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Progressive four-course lunch and pairing with a wine expert</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">168 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$155</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Discover a local foodie paradise</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">49 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$21</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Join a relaxed bike tour along a beautiful river</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">51 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$147</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Sweat it out with a street dance pro</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">99 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$27</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Drink Tea. Slow Down. Connect. Feel Ordinary Magic</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">115 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$72</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Great live jazz, beautiful setting.</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">125 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$123</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Original Cookie Making Workshop</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">144 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$107</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Blood and Tears: Horror Walking Tour</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">92 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$153</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See London By Night Bus Tour</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">113 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$196</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">London Creative Photography Course</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">115 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$165</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See top London sites whilst learning the ukulele</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">114 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$130</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Make macarons with a top pastry chef</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">114 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$74</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Meet London artists and create modern art</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">112 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$152</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">See the city with a social running group</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                                <li>
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">93 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$192</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Learn to make sushi and poke with a chef</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">9 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$95</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 ">
                  <div class="theme-search-results-item _br-3 _mb-10 theme-search-results-item-bs theme-search-results-item-lift theme-search-results-item-grid">
                    <div class="banner _h-20vh _h-mob-30vh banner-">
                      <div class="banner-bg" style="background-image:url(./img/315x225.png);"></div>
                    </div>
                    <div class="theme-search-results-item-grid-body">
                      <a class="theme-search-results-item-mask-link" href="#"></a>
                      <div class="theme-search-results-item-grid-header">
                        <h5 class="theme-search-results-item-title">Enjoy a private show then the magician will divulge his secrets</h5>
                      </div>
                      <div class="theme-search-results-item-grid-caption">
                        <div class="row" data-gutter="10">
                          <div class="col-xs-9 ">
                            <div class="theme-search-results-item-rating">
                              <ul class="theme-search-results-item-rating-stars">
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                                <li class="active">
                                  <i class="fa fa-star"></i>
                                </li>
                              </ul>
                              <p class="theme-search-results-item-rating-title">74 reviews</p>
                            </div>
                          </div>
                          <div class="col-xs-3 ">
                            <div class="theme-search-results-item-price">
                              <p class="theme-search-results-item-price-tag">$79</p>
                              <p class="theme-search-results-item-price-sign">person</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>--%>
                        </div>
                        <div class="theme-search-results-mobile-filters" id="mobileFilters">
                            <a class="theme-search-results-mobile-filters-btn magnific-inline" href="#MobileFilters">
                                <i class="fa fa-filter"></i>Filters
                </a>
                            <div class="magnific-popup mfp-hide" id="MobileFilters">
                                <div class="theme-search-results-sidebar">
                                    <div class="theme-search-results-sidebar-sections">
                                        <div class="theme-search-results-sidebar-section">
                                            <h5 class="theme-search-results-sidebar-section-title">Type</h5>
                                            <div class="theme-search-results-sidebar-section-checkbox-list">
                                                <div class="theme-search-results-sidebar-section-checkbox-list-items">
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Immersions
                                 
                                                                <span class="icheck-sub-title">Happen over multiple days</span>
                                                            </span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">189</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Experiences
                                 
                                                                <span class="icheck-sub-title">Last 2 or more hours</span>
                                                            </span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">106</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="theme-search-results-sidebar-section">
                                            <h5 class="theme-search-results-sidebar-section-title">Price</h5>
                                            <div class="theme-search-results-sidebar-section-price">
                                                <input id="price-slider-mob" name="price-slider" data-min="100" data-max="500" />
                                            </div>
                                        </div>
                                        <div class="theme-search-results-sidebar-section">
                                            <h5 class="theme-search-results-sidebar-section-title">Review Score</h5>
                                            <div class="theme-search-results-sidebar-section-checkbox-list">
                                                <div class="theme-search-results-sidebar-section-checkbox-list-items">
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Excellent</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">338</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Good</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">168</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Okay</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">341</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Mediocre</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">316</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Poor</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">165</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="theme-search-results-sidebar-section">
                                            <h5 class="theme-search-results-sidebar-section-title">Category</h5>
                                            <div class="theme-search-results-sidebar-section-checkbox-list">
                                                <div class="theme-search-results-sidebar-section-checkbox-list-items">
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Arts</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">482</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Business</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">446</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Entertainment</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">238</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Fashion</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">190</span>
                                                    </div>
                                                </div>
                                                <div class="collapse" id="mobile-SearchResultsCheckboxCategory">
                                                    <div class="theme-search-results-sidebar-section-checkbox-list-items theme-search-results-sidebar-section-checkbox-list-items-expand">
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Food &amp; Drink</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">114</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">History</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">342</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Music</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">196</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Lifestyle</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">403</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Nature</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">399</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Nightlife</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">406</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Social Impact</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">444</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Sports</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">134</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Technology</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">351</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Wellness</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">266</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="theme-search-results-sidebar-section-checkbox-list-expand-link" role="button" data-toggle="collapse" href="#mobile-SearchResultsCheckboxCategory" aria-expanded="false">Show more
                           
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="theme-search-results-sidebar-section">
                                            <h5 class="theme-search-results-sidebar-section-title">Neighborhoods</h5>
                                            <div class="theme-search-results-sidebar-section-checkbox-list">
                                                <div class="theme-search-results-sidebar-section-checkbox-list-items">
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Westminster</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">385</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Kensington</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">424</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Camden</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">133</span>
                                                    </div>
                                                    <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                        <label class="icheck-label">
                                                            <input class="icheck" type="checkbox" />
                                                            <span class="icheck-title">Hammersmith</span>
                                                        </label>
                                                        <span class="theme-search-results-sidebar-section-checkbox-list-amount">140</span>
                                                    </div>
                                                </div>
                                                <div class="collapse" id="mobile-SearchResultsCheckboxNeighborhoods">
                                                    <div class="theme-search-results-sidebar-section-checkbox-list-items theme-search-results-sidebar-section-checkbox-list-items-expand">
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Southwark</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">107</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Tower Hamlets</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">393</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Islington</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">116</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">City of London</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">349</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Lambeth</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">120</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Hackney</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">191</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Greenwich</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">348</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Wandsworth</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">184</span>
                                                        </div>
                                                        <div class="checkbox theme-search-results-sidebar-section-checkbox-list-item">
                                                            <label class="icheck-label">
                                                                <input class="icheck" type="checkbox" />
                                                                <span class="icheck-title">Lewisham</span>
                                                            </label>
                                                            <span class="theme-search-results-sidebar-section-checkbox-list-amount">465</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <a class="theme-search-results-sidebar-section-checkbox-list-expand-link" role="button" data-toggle="collapse" href="#mobile-SearchResultsCheckboxNeighborhoods" aria-expanded="false">Show more
                           
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="btn _tt-uc _fs-sm btn-white btn-block btn-lg" href="#">Load More Results</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

