﻿<%@ Page Language="VB" AutoEventWireup="true" CodeFile="regs_new.aspx.vb" Inherits="regs_new" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700" rel="stylesheet" />
    <title>Agency Registration</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="Registration/Content/User/animation.css" rel="stylesheet" />
    <script src="../ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="../ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.7.2.js"></script>
    <script src="Registration/Scripts/External/angular.min.js"></script>
    <script src="Registration/Scripts/Js/jquery-ui.js"></script>
    <script src="Registration/Scripts/Js/User/adminJs4730.js?v=637071017756558079"></script>
    <script src="Registration/Scripts/Js/User/AnguEmail7df5.js?v=637088359368128168"></script>
    <link href="Registration/Content/CSS/themes/jquery.ui.datepickeref06.css?v=636490425933074891" rel="stylesheet" />
    <link href="../code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="Advance_CSS/css/bootstrap.css" />
    <link rel="stylesheet" href="Advance_CSS/css/styles.css" />
    <link href="CSS/login/logincss.css" rel="stylesheet" />
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <form runat="server" class="rcw-form container-fluid ff-box-shadow">
        <div class="container-fluid" runat="server" id="table_reg">
            <div class="row justify-content-center">
                <div class="col-11 col-sm-10 col-md-10 col-lg-10 col-xl-5 text-center p-0 mt-3 mb-2">
                    <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                        <h2 id="heading">Sign Up Your User Account</h2>
                        <p>Fill all form field to go to next step</p>
                        <div class="stepwizard">
                            <div class="stepwizard-row setup-panel">
                                <div class="stepwizard-step step1">
                                    <span class="btn btn-primary btn-circle" >1</span>
                                    <p>Personal Details</p>
                                </div>
                                <div class="stepwizard-step step2">
                                    <span class="btn btn-default btn-circle step2circle" >2</span>
                                    <p>Agency & Business Details</p>
                                </div>
                                <div class="stepwizard-step step3">
                                    <span  class="btn btn-default btn-circle step3circle" >3</span>
                                    <p>Authentication</p>
                                </div>

                            </div>
                        </div>
                        <div class="box">
                            <div class="row setup-content" id="step-1">
                                <div class="col-xs-12">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-substitute"></i>
                                                    <asp:DropDownList ID="ddlATitle" CssClass="theme-search-area-section-input" placeholder="Title" runat="server">
                                                        <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                                        <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                                        <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner fistname">
                                                    <i class="theme-search-area-section-icon icofont-id"></i>
                                                    <asp:TextBox ID="txtAFirstName" CssClass="theme-search-area-section-input" runat="server" placeholder="First Name *" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner lastname">
                                                    <i class="theme-search-area-section-icon icofont-id"></i>
                                                    <asp:TextBox ID="txtALastName" CssClass="theme-search-area-section-input" placeholder="Last Name *" runat="server" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner mobileno">
                                                    <i class="theme-search-area-section-icon icofont-ipod-touch"></i>
                                                    <asp:TextBox ID="txtAMobileNo" CssClass="theme-search-area-section-input" onKeyUp="checkUserIdStrength()" onkeypress="return isNumber(event);" placeholder="Mobile No. * (Your user id )" runat="server" MaxLength="10"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner emailid">
                                                    <i class="theme-search-area-section-icon icofont-ui-email"></i>
                                                    <asp:TextBox ID="txtAEmailId" CssClass="theme-search-area-section-input" placeholder="Email *" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-ipod-touch"></i>
                                                    <asp:TextBox ID="txtAPhoneNo" CssClass="theme-search-area-section-input" placeholder="Phone No. (Optional)" runat="server" MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner whatsupno">
                                                    <i class="theme-search-area-section-icon icofont-brand-whatsapp"></i>
                                                    <asp:TextBox ID="txtAWhatupNo" CssClass="theme-search-area-section-input" placeholder="Whatsapp No. *" onkeypress="return isNumber(event);" runat="server" MaxLength="15"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-ui-email"></i>
                                                    <asp:TextBox ID="txtAOptEmailId" CssClass="theme-search-area-section-input" runat="server" placeholder="Email 2(Optional)"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-fax"></i>
                                                    <asp:TextBox ID="txtAFaxNo" CssClass="theme-search-area-section-input" placeholder="Fax No.(Optional)" runat="server" Style="position: static"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="btn btn-primary pull-right" id="Step1Next" style="height: 30px; line-height: 15px;">Next >></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row setup-content" id="step-2" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner agencyname">
                                                    <i class="theme-search-area-section-icon icofont-building"></i>
                                                    <asp:TextBox ID="txtAAgencyName" CssClass="theme-search-area-section-input" runat="server" placeholder="Agency Name *" MaxLength="30"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner agencyaddress">
                                                    <i class="theme-search-area-section-icon icofont-map-pins"></i>
                                                    <asp:TextBox ID="txtAAgencyAddress" CssClass="theme-search-area-section-input" runat="server" placeholder="Agency Address *" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-map"></i>
                                                    <asp:DropDownList ID="ddlCountryList" CssClass="theme-search-area-section-input" runat="server" AutoPostBack="True">
                                                        <asp:ListItem Selected="True" Value="India">India</asp:ListItem>
                                                        <asp:ListItem Value="Other">Other</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="Coun_txt" CssClass="psb_dd form-control " runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner statelist">
                                                    <i class="theme-search-area-section-icon icofont-map"></i>
                                                    <asp:DropDownList ID="ddlAStateList" CssClass="theme-search-area-section-input" runat="server" onchange="javascript:ChangeOtherStateEvent();">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="Stat_txt" CssClass="psb_dd form-controlrt" runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-building-alt"></i>
                                                    <asp:TextBox ID="TextBox_Area" CssClass="theme-search-area-section-input" runat="server" placeholder="District *" MaxLength="20" Style="position: static"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-map"></i>
                                                    <asp:TextBox ID="Other_City" CssClass="theme-search-area-section-input " runat="server" Visible="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner citylist">
                                                    <i class="theme-search-area-section-icon icofont-building-alt"></i>
                                                    <asp:DropDownList ID="ddlACityList" CssClass="theme-search-area-section-input" runat="server"  onchange="javascript:SelCityValue();"></asp:DropDownList>
                                                    <asp:HiddenField ID="hdnASelCity" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner pincode">
                                                    <i class="theme-search-area-section-icon icofont-search-map"></i>
                                                    <asp:TextBox ID="txtAPinCode" CssClass="theme-search-area-section-input" runat="server" placeholder="Pin Code" MaxLength="6" onkeypress="return isNumber(event);"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-ui-v-card"></i>
                                                    <asp:TextBox ID="Web_txt" CssClass="theme-search-area-section-input" placeholder="Business WebSite" runat="server" Style="display: none"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner pancardno">
                                                    <i class="theme-search-area-section-icon icofont-ui-v-card"></i>
                                                    <asp:TextBox ID="txtPanNumber" Style="text-transform: uppercase;" CssClass="theme-search-area-section-input" placeholder="PAN No. *" MaxLength="10" onkeypress="return isAlphaNumeric(event);" onkeyup="return CheckPanNumber();" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner nameonpan">
                                                    <i class="theme-search-area-section-icon icofont-ui-v-card"></i>
                                                    <asp:TextBox ID="txtANameOnPan" CssClass="theme-search-area-section-input" placeholder="Name On PAN *" runat="server" MaxLength="40"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div runat="server" visible="false">
                                            <asp:DropDownList ID="Stat_drop" CssClass="input_bx" runat="server" Height="20px" Width="150px">
                                                <asp:ListItem Value="TA" Selected="True">Travel Agent</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-bag-alt"></i>
                                                    <asp:DropDownList ID="Sales_DDL" CssClass="theme-search-area-section-input" runat="server" class="input_bx">
                                                        <asp:ListItem Value="--Refer By--">--Refer By--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner">
                                                    <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                    <asp:TextBox ID="Rem_txt" CssClass="theme-search-area-section-input" placeholder="Remark (Optional)" runat="server" Style="position: static"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-4">
                                            <%-- <div class="file-upload-wrapper" data-text="Upload PAN Card">--%>
                                            <div data-text="Upload PAN Card">
                                                <h5 style="text-align: left;">Upload pan card document *</h5>
                                                <asp:FileUpload ID="fluPanCard" runat="server" CssClass="file-upload-field panfileupload" title="Pancard image must be in JPG formate" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div data-text="Upload Address Proof">
                                                 <h5 style="text-align: left;">Upload address proof document *</h5>
                                                <asp:FileUpload ID="fluCompAddress" runat="server" CssClass="file-upload-field comaddressproof" title="Company address image must be in JPG formate" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div  data-text="Upload Agency Logo">
                                                <h5 style="text-align: left;">Upload agency logo</h5>
                                                <asp:FileUpload ID="fluAgencyLogo" name="file-upload-field" runat="server" class="file-upload-field" value="" title="Logo must be in PNG formate and  Size should be (90*70) pixels" />
                                            </div>
                                        </div>
                                        <div class="input-group col_2 revealOnScroll" data-animation="fadeInUp" runat="server" visible="false">
                                            <asp:DropDownList ID="DropDownList1" runat="server" CssClass="input_bx" Style="display: none"></asp:DropDownList>
                                        </div>
                                        <asp:DropDownList ID="DD_Branch" runat="server" CssClass="input_bx" Visible="false"></asp:DropDownList>
                                        <div class="clearfix"></div><br />
                                        <div class="col-md-12" data-animation="fadeInUp">
                                            <p style="color: #000; text-align: left;">
                                                Do you have GST information ? &nbsp;
                                                <asp:CheckBox ID="chkIsGST" runat="server" CssClass="dohavegst" onclick="CheckGStIsCheckOrNot();" />
                                            </p>
                                        </div>
                                        <div class="row" id="divGSTInformation" runat="server" style="display: none;">
                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstnumber">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGstNumber" CssClass="theme-search-area-section-input"  Style="text-transform: uppercase;" MaxLength="15" placeholder="GST No. *" runat="server" onkeypress="return isAlphaNumeric(event);" onkeyup="return CheckGSTNumber();"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstcomname">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGSTCompanyName" CssClass="theme-search-area-section-input" placeholder="Company Name *" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstcomaddress">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGSTCompanyAddress" CssClass="theme-search-area-section-input" placeholder="Company Address *" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gststate">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:DropDownList ID="ddlGSTState" CssClass="theme-search-area-section-input" runat="server" class="input_bx" onchange="javascript:ChangeGSTStateEvent();">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstcity">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:DropDownList ID="ddlGSTCity" CssClass="theme-search-area-section-input" runat="server" class="input_bx" onchange="javascript:SelGstCityValue();">
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="hdnGSTCity" runat="server" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstpincode">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGSTPincode" CssClass="theme-search-area-section-input" placeholder="Pin Code *" runat="server" MaxLength="6" onkeypress="return isNumber(event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGSTPhoneNo" CssClass="theme-search-area-section-input" placeholder="Phone No" runat="server" onkeypress="return isNumber(event);"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="theme-search-area-section theme-search-area-section-line">
                                                    <div class="theme-search-area-section-inner gstemailid">
                                                        <i class="theme-search-area-section-icon icofont-edit-alt"></i>
                                                        <asp:TextBox ID="txtGSTEMailId" CssClass="theme-search-area-section-input" placeholder="Email Id *" runat="server" ></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <span class="btn btn-primary pull-left" id="Step1Back" style="height: 30px; line-height: 15px;"><< Back</span>
                                        <span class="btn btn-primary pull-right" id="Step2Next" style="height: 30px; line-height: 15px;">Next >></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row setup-content" id="step-3" style="display: none;">
                                <div class="col-xs-12">
                                    <div class="col-md-12">
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner auserid">
                                                    <i class="theme-search-area-section-icon icofont-user-male"></i>
                                                    <%--<asp:TextBox ID="TxtUserIdreg" value="{{name}}" runat="server" Style="position: static" MaxLength="20" CssClass="theme-search-area-section-input" placeholder="User Id *" ReadOnly="true"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtUserId" runat="server" Style="position: static" MaxLength="20" CssClass="theme-search-area-section-input" placeholder="User Id *" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner auserpass">
                                                    <i class="theme-search-area-section-icon icofont-lock"></i>
                                                    <asp:TextBox ID="txtUserPassword" runat="server" Style="position: static"
                                                        TextMode="Password" MaxLength="16" CssClass="theme-search-area-section-input" placeholder="Password *"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-3">
                                            <div class="theme-search-area-section theme-search-area-section-line">
                                                <div class="theme-search-area-section-inner auserconfpass">
                                                    <i class="theme-search-area-section-icon icofont-lock"></i>
                                                    <asp:TextBox ID="txtUserConfPassword" CssClass="theme-search-area-section-input" placeholder="Confirm Password *" runat="server" Style="position: static"
                                                        TextMode="Password" MaxLength="16"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div><br />
                                         <span class="btn btn-primary pull-left" id="Step2Back" style="height: 30px; line-height: 15px;"><< Back</span>

                                        <asp:Button ID="btnRegSubmit" runat="server" Text="Register" CssClass="btn btn-success pull-right" OnClientClick="return CheckAgencyAuthVal();" />


                                        <%--<button class="btn btn-success pull-right" type="submit">Finish!</button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default navbar-inverse navbar-theme navbar-theme-abs navbar-theme-transparent navbar-theme-border" id="main-nav">
            <div class="container">
                <div class="navbar-inner nav">
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" data-target="#navbar-main" data-toggle="collapse" type="button" area-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/flight-search">
                            <img src="Advance_CSS/Icons/logo(ft).png" alt="Image Alternative text" title="Image Title" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-main">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="about-us.html" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">HOME</a>
                            </li>
                            <li>
                                <a href="contact-us.aspx">CONTACT US</a>
                            </li>

                            <li>
                                <a href="contact-us.aspx">PARTNER SOLUTIONS</a>
                            </li>

                            <li>
                                <a href="contact-us.aspx">OFFERS</a>
                            </li>

                            <li>
                                <a href="contact-us.aspx">KNOWLEDGE BASE</a>
                            </li>

                            <li>
                                <a href="contact-us.aspx">BLOG</a>
                            </li>
                        </ul>



                        <ul class="nav navbar-nav navbar-right" style="margin-top: 8px;">
                            <li style="margin-right: 17px;">
                                <a style="background-color: #674299;border-radius: 4px;color: #fff;display: inline-block;font-family: Helvetica, Arial, sans-serif;font-size: 13px;font-weight: normal;line-height: 0px;text-align: center;text-decoration: none;width: 100px;-webkit-text-size-adjust: none;height: 10px;" href="Login.aspx">Login Here</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="theme-page-section theme-page-section-gray theme-page-section-lg" runat="server" visible="false" style="margin-top: 40px;">
            <div class="container">
                <div id="val" style="display: none; text-align: center; font-size: 20px; color: rgb(229 0 0); background-color: rgb(238 207 207); padding: 8px; margin-bottom: 6px; border-radius: 6px;"></div>
                <div id="divErrorMsg" runat="server" visible="false" style="font-size: 20px; text-align: center; color: rgb(229 0 0); background-color: rgb(238 207 207); padding: 8px; margin-bottom: 6px; border-radius: 6px;"></div>
            </div>
        </div>
    </form>
    <div id="table_Message" runat="server" visible="false" style="margin-top: 6%;">
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Nunito);

            /* Take care of image borders and formatting */

            img {
                max-width: 600px;
                outline: none;
                text-decoration: none;
                -ms-interpolation-mode: bicubic;
            }

            html {
                margin: 0;
                padding: 0;
            }

            a {
                text-decoration: none;
                border: 0;
                outline: none;
                color: #bbbbbb;
            }

                a img {
                    border: none;
                }

            /* General styling */

            td, h1, h2, h3 {
                font-family: Helvetica, Arial, sans-serif;
                font-weight: 400;
            }

            td {
                text-align: center;
            }

            body {
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none;
                width: 100%;
                height: 100%;
                color: #666;
                background: #fff;
                font-size: 16px;
                height: 100vh;
                width: 100%;
                padding: 0px;
                margin: 0px;
            }

            table {
                border-collapse: collapse !important;
            }

            .headline {
                color: #444;
                font-size: 25px;
            }

            .force-full-width {
                width: 100% !important;
            }
        </style>


        <style media="screen" type="text/css">
            @media screen {
                td, h1, h2, h3 {
                    font-family: 'Nunito', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                }
            }
        </style>
        <style media="only screen and (max-width: 480px)" type="text/css">
            /* Mobile styles */
            @media only screen and (max-width: 480px) {

                table[class="w320"] {
                    width: 320px !important;
                }
            }
        </style>


        <body bgcolor="#fff" class="body" style="padding: 20px; margin: 0; display: block; background: #ffffff; -webkit-text-size-adjust: none">
            <table align="center" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tbody>
                    <tr>
                        <td align="center" bgcolor="#fff" class="" valign="top" width="100%">
                            <center class="">
                          <table cellpadding="0" cellspacing="0" class="w320" style="margin: 0 auto;" width="600">
                              <tbody>
                                  <tr>
                                      <td align="center" class="" valign="top">
                                          <table cellpadding="0" cellspacing="0" style="margin: 0 auto;" width="100%">
                                          </table>
                                          <table bgcolor="#fff" cellpadding="0" cellspacing="0" class="" style="margin: 0 auto; width: 100%;">
                                              <tbody style="margin-top: 15px;">
                                                  <tr class="">
                                                      <td class="">
                                                          <img alt="robot picture" class="" height="155" src="../Advance_CSS/Icons/logo(ft).png" style="height: 75px;">
                                                      </td>
                                                  </tr>
                                                  <tr class="">
                                                      <td class="headline">Welcome to Richa Travels!</td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                                          <center class="">
                                                              <table cellpadding="0" cellspacing="0" class="" style="margin: 0 auto;" width="100%">
                                                                  <tbody class="">
                                                                      <tr class="">
                                                                          <td class="" style="color: #444; font-weight: 400; text-align: left;">                                                                             
                                                                              <br>
                                                                              <p>Dear Sir</p>
                                                                              <br>
                                                                              <p>Thank you for registering with www.Richa Travels.com</p>
                                                                              <%--<p>Your Agency ID </p>--%>
                                                                              <p>Your User ID : <%=CID%></p>
                                                                              <p>Your Password : <%=Password%></p>
                                                                              <p>Your registered mobile is : <%=Mobile%></p>
                                                                              <p>Your registered email is  : <%=Email %></p>
                                                                              <p>We will activate your portal account within 24 hours after thorough verification. For any assistance, please call on +91 93 3192 2333 Or email on sales@Richa Travels.com</p>
                                                                              <br>

                                                                              <p>Thanks</p>
                                                                              <p>Team Richa Travels</p>



                                                                              <br />
                                                                              <%-- Your login credentials will be sent on your register mail id & mobile number, after activation of your agency account.

    <br />
    <br />
    Our backend team will activate your account within 24 hour, for further process & support you can contact us on +91 79 4910 9999

 <br>
  Your registration reference id is :
<br>
<span style="font-weight:bold;">User Id: &nbsp;</span><span style="font-weight:lighter;" class=""><%=CID%></span> 
 <br>
  <span style="font-weight:bold;">Password: &nbsp;</span><span style="font-weight:lighter;" class=""><%=Password%></span>--%>
                                                                             
                                                                          </td>
                                                                      </tr>
                                                                  </tbody>
                                                              </table>
                                                          </center>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td class="">
                                                          <div class="">
                                                              <a style="background-color: #674299; border-radius: 4px; color: #fff; display: inline-block; font-family: Helvetica, Arial, sans-serif; font-size: 18px; font-weight: normal; line-height: 50px; text-align: center; text-decoration: none; width: 350px; -webkit-text-size-adjust: none;" href="/">Thank You</a>
                                                          </div>
                                                          <br>
                                                      </td>
                                                  </tr>
                                              </tbody>

                                          </table>

                                        <%--  <table bgcolor="#fff" cellpadding="0" cellspacing="0" class="force-full-width" style="margin: 0 auto; margin-bottom: 5px:">
                                              <tbody>
                                                  <tr>
                                                      <td class="" style="color: #444;">
                                                          <p>
                                                              The password was auto-generated, however feel free to change it 
  
    <a href="" style="text-decoration: underline;">here</a>

                                                          </p>
                                                      </td>
                                                  </tr>
                                              </tbody>
                                          </table>--%>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                      </center>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width='450' border='0' cellspacing='0' cellpadding='0' height='84' style='background-color: #0e4ca1; display: none; border-radius: 15px; color: #FFF; font-family: Arial, Helvetica, sans-serif; font-size: 22px;'>
                <tr align='center' valign='middle'>
                    <td>USER NAME:  <%=CID%> </td>
                </tr>
                <tr align='center' valign='middle'>
                    <td>PASSWORD: <%=Password%></td>
                </tr>

            </table>
        </body>

    </div>

    <script>
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script src="Registration/Scripts/Js/User/custom-file-input.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="JS/login/loginjs.js"></script>
</body>
</html>
