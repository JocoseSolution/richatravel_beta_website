﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage_Test.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section>
        <div class="rows contact-map map-container">
            <div style="width: 100%; position: relative; margin-top: 70px;">

                <div style="width: 100%; position: relative;">
                    <iframe width="100%" height="300" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14688.004756131944!2d72.54170592747269!3d23.023728559237814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e8458fcd9dd7b%3A0x437683b8114be6d8!2sRicha%20World%20Travels!5e0!3m2!1sen!2sin!4v1638178440736!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					
                    <div style="position: absolute; width: 80%; bottom: 10px; left: 0; right: 0; margin-left: auto; margin-right: auto; color: #000; text-align: center;"><small style="line-height: 1.8; font-size: 2px; background: #fff;">Powered by <a href="http://www.googlemapsgenerator.com/ja/">Googlemapsgenerator.com/ja/</a> & <a href="https://www.cusl.se/">cusl.se</a></small></div>
                    <style>
                        #gmap_canvas img {
                            max-width: none !important;
                            background: none !important
                        }
                    </style>
                </div>
                <br />



            </div>
    </section>

    <section>
        <div class="form form-spac rows con-page">
            <div class="container">
                <!-- TITLE & DESCRIPTION -->
                <div class="spe-title col-md-12">
                    <h2><span>Contact us</span></h2>
                    <div class="title-line">
                        <div class="tl-1"></div>
                        <div class="tl-2"></div>
                        <div class="tl-3"></div>
                    </div>
                    <%--					<p>World's leading tour and travels Booking website. Book flight tickets and enjoy your holidays with distinctive experience</p>--%>
                </div>

                <div class="pg-contact">

                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                        <img src="img/contact/1.html" alt="">
                        <h4>Richa World Travels</h4>
                        <p>
                            Richa World Travels is more than just a website or company. Richa World Travels is for belief of agents that every agents has for their travelers, to distinctive experience of their travel and to grow.
                        </p>

                    </div>
					<div class="col-md-3 col-sm-6 col-xs-12 new-con new-con1">
                        <img src="img/contact/1.html" alt="">
                        <h4>Address</h4>
                        <p>
                            301, Hare Krishna Complex, Behind City Gold Theater, Ashram Road
Ahmedabad - 380009
                        </p>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3">
                        <img src="img/contact/2.html" alt="">
                        <h4>Contact Information
                        </h4>
                        <p>Phone : +91 79 4910 9999</p>
                        <p>Email : info@richaworldtravels.com</p>
                    </div>
					<div class="col-md-3 col-sm-6 col-xs-12 new-con new-con3">
                        <img src="img/contact/2.html" alt="">
                        <h4>Website
                        </h4>
                        <p>Url : www.richatravels.in</p>
                    </div>
					
                    <%--<div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4">
                        <img src="img/contact/3.html" alt="">
                        <h4>Sales Support</h4>
                        <p>Phone :  (011) 43554899</p>
                        <p>
                            Email : www.richatravels.in
                            
                        </p>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12 new-con new-con4">
                        <img src="img/contact/3.html" alt="">
                        <h4>Accounts & Balance Uploads </h4>
                        <p>Mhone : (011) 43554899 </p>
                        <p>Email : admin@flyagainonline.net</p>
                    </div>--%>
                </div>
            </div>
        </div>
    </section>




</asp:Content>

