﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Drawing;
using System.IO;
using System.Web.Services;
public partial class Package_Package_Details : System.Web.UI.Page
{
    private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        string pkg_counter = Request.QueryString["Pkg_Id"];
        BindAllPackage(pkg_counter);
        BindPackageTitle(pkg_counter);

    }
    protected void BindAllPackage(string counter)
    {
        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetSelectedPackageDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", counter);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PackageDetails.DataSource = dt;
            PackageDetails.DataBind();

            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }

    protected void BindPackageTitle(string counter)
    {
        try
        {
            con.Open();
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand("GetSelectedPackageDetails", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", counter);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            PkgTitle.DataSource = dt;
            PkgTitle.DataBind();

            con.Close();
        }
        catch (Exception ex)
        {
            con.Close();
        }
    }
    [WebMethod]
    public static string SendEmailFromPortal(string name, string mobile, string email, string address, string notes, string pkgid)
    {
        string rutStr = string.Empty;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString.ToString());
        try
        {
            con.Open();
            String sp = "InsertPackageBooking";
            SqlCommand cmd = new SqlCommand(sp, con);
            //string pkgid = HttpContext.Current.Request.QueryString["Pkg_Id"];
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@pkgid", pkgid);
            cmd.Parameters.AddWithValue("@fullname", name);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@mobile", mobile);
            cmd.Parameters.AddWithValue("@address", address);
            cmd.Parameters.AddWithValue("@notes", notes);

            cmd.Parameters.AddWithValue("@bookingdate", DateTime.Now);
            // string body = "<p>"+ name + "</p>"+ "<p>" + mobile + "</p>"+ "<p>" + address + "</p>"+ "<p>" + notes + "</p>";
            //string ToClientBody = EmlTemp1();
            //string ToClienAdmintBody = EmlTemp2();
            //ToClienAdmintBody = ToClienAdmintBody.Replace("#Name", name).Replace("#email", email).Replace("#mob", mobile).Replace("#pkgid", pkgid).Replace("#address", address).Replace("#notes", notes);
            string subject1 = "Thank you " + name;
            string subject2 = "New Package Booking " + name;
            if (cmd.ExecuteNonQuery() > 0)
            {
                rutStr = "success";
                //int n = SendMail1(email, "Info@faremonster.in", "smtp.gmail.com", ToClientBody, subject1);
                //int i = SendMail1("Info@faremonster.in", "Info@faremonster.in", "smtp.gmail.com", ToClienAdmintBody, subject2);
                //if (n == 1)
                //{

                //}
            }
            con.Close();

        }
        catch (Exception ex)
        {
            con.Close();
            rutStr = "fail";
        }
        return rutStr;
    }
}