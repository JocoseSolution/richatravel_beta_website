﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

public partial class Package_Package_Home : System.Web.UI.Page
{
    private SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        GetAllDomesticPackage();
        GetAllInternationalPackage();
    }
    public void GetAllDomesticPackage()
    {
        // Dim Connection As New SqlConnection(conn)
        try
        {
            con.Open();
            string SP = "SELECT TOP 4 * FROM Pkg_Details Where Pkg_Type='Domestic'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(SP, con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            DomesticPkg.DataSource = dt;
            DomesticPkg.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
        }
    }
    public void GetAllInternationalPackage()
    {
        // Dim Connection As New SqlConnection(conn)
        try
        {
            con.Open();
            string SP = "SELECT TOP 4 * FROM Pkg_Details Where Pkg_Type='International'";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(SP, con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            InetrnationalPkg.DataSource = dt;
            InetrnationalPkg.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
        }
    }

    public void GetAllPackage()
    {
        // Dim Connection As New SqlConnection(conn)
        try
        {
            con.Open();
            string SP = "SELECT * FROM Pkg_Details Where pkg_status=1";
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand(SP, con);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);

            InetrnationalPkg.DataSource = dt;
            InetrnationalPkg.DataBind();
            con.Close();
        }
        catch (Exception ex)
        {
        }
    }





    protected void Search_Click(object sender, EventArgs e)
    {
        if(TxtDestination.Text !="")
        {
            string src = TxtDestination.Text;
            Response.Redirect("Package_List.aspx?PkgType=Search & SearchQuery=" + src +"");
        }
        else
        {
            Response.Redirect("Package_List.aspx?PkgType=All");

        }

    }
}