﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" MasterPageFile="~/MasterForHome.master" %>

<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="Search" TagName="HotelSearch" %>
<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="Searchsss" %>
<%@ Register Src="~/UserControl/FltSearchFixDep.ascx" TagName="IBESearchDep" TagPrefix="SearchDep" %>
<%@ Register Src="~/UserControl/Fare_Cal.ascx" TagName="FareCal" TagPrefix="FDCAL" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">
    <link type="text/css" href="Advance_CSS/dropdown_search_box/css/select2.min.css" rel="stylesheet" />
    <link href="Advance_CSS/css/Search.css" rel="stylesheet" />
    <link type="text/css" href="Custom_Design/css/search.css" rel="stylesheet" />
    <link href="Advance_CSS/css/Fair-Calander.css" rel="stylesheet" />
    <link href="icofont/icofont.css" rel="stylesheet" />
    <link href="icofont/icofont.min.css" rel="stylesheet" />
    <%--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>--%>

    <style type="text/css">
        .tab__list {
            display:none !important;
        }

        #resource-slider {
            position: relative;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 13em;
            margin: auto;
           
            overflow: hidden;
        }
.bootstrap-select.btn-group .dropdown-menu {
    min-width: 108% !important;
    z-index: 1035;
    margin-left: -40px  !important;
    margin-top: -43px  !important;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
            #resource-slider .arrow {
                cursor: pointer;
                position: absolute;
                width: 2em;
                height: 0;
                padding: 0;
                margin: 0;
                outline: 0;
                background: transparent;
            }

                #resource-slider .arrow:hover {
                    background: rgba(0, 0, 0, 0.1);
                }

                #resource-slider .arrow:before {
                    content: '';
                    position: absolute;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    width: 0.75em;
                    height: 0.75em;
                    margin: auto;
                    border-style: solid;
                }

            #resource-slider .prev {
                left: 0;
                bottom: 0;
            }

                #resource-slider .prev:before {
                    left: 0.25em;
                    border-width: 3px 0 0 3px;
                    border-color: #333 transparent transparent #333;
                    transform: rotate(-45deg);
                }

            #resource-slider .next {
                right: 0;
                bottom: 0;
            }

                #resource-slider .next:before {
                    right: 0.25em;
                    border-width: 3px 3px 0 0;
                    border-color: #333 #333 transparent transparent;
                    transform: rotate(45deg);
                }

            #resource-slider .resource-slider-frame {
                position: absolute;
                top: 0;
                left: 2em;
                right: 2em;
                bottom: 0;
                border-left: 0.25em solid transparent;
                border-right: 0.25em solid transparent;
                overflow: hidden;
            }

            #resource-slider .resource-slider-item {
                position: absolute;
                top: 0;
                bottom: 0;
                width: 25%;
                height: 100%;
            }

            #resource-slider .resource-slider-inset {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                margin: 0.5em 0.25em;
                /*overflow: hidden;*/
            }

        @media ( max-width: 60em ) {
            #resource-slider .resource-slider-item {
                width: 33.33%;
            }

            #resource-slider {
                height: 16em;
            }
        }

        @media ( max-width: 45em ) {
            #resource-slider .resource-slider-item {
                width: 50%;
            }
        }

        @media ( max-width: 30em ) {
            #resource-slider .resource-slider-item {
                width: 100%;
            }

            #resource-slider {
                height: 19em;
            }
        }

        .new_ofr_hp {
    border-radius: 4px;
}

.new_ofr_hp {
    width: 253px !important;
    float: left;
    border: 1px solid #bdbaba00;
    cursor: pointer;
    /*min-height: 235px;*/
    position: relative;
    background: white;
}

.new_ofr_hp_img {
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    overflow: hidden;
}

.new_ofr_hp_img {
    width: 251px;
    height: 119px;
    float: left;
    position: relative;
}

.new_hp_text {
    width: 92%;
    margin: 0 auto;
    text-align: left;
    display: table;
}

    .new_hp_text h3 {
        font-size: 14px;
        color: #000;
        font-weight: 400;
        margin: 8px 0 6px;
    }

.hp_pra {
    font-size: 12px;
    color: #000;
    font-weight: 400;
    margin: 0 0 7px;
    text-align: justify;
}

.hp_vld {
    font-size: 15px;
    color: #d94a31;
    font-weight: 600;
    margin: -3px 0;
    text-align: right;
    position: absolute;
    bottom: 7px;
    right: 7px;
}

.promo-sc {
    border: 1px dashed #ef6614;
    font-size: 11px;
    padding: 1px 2px;
    width: 37%;
    text-align: center;
    position: absolute;
    bottom: 7px;
    left: 7px;
}

.promo-ttl {
    color: #ef6614;
    font-weight: 600;
}

.promo-cd {
    color: #000;
    font-weight: 600;
}

.ribbon {
    position: absolute;
    left: -5px;
    top: -5px;
    z-index: 1;
    overflow: hidden;
    width: 75px;
    height: 75px;
    text-align: right;
}

    .ribbon p {
        font-size: 10px;
        font-weight: bold;
        color: #FFF;
        text-transform: uppercase;
        text-align: center;
        line-height: 20px;
        transform: rotate( -45deg );
        -webkit-transform: rotate( -45deg );
        width: 100px;
        display: block;
        background: #79A70A;
        background: linear-gradient(#F70505 0%, #8F0808 100%);
        box-shadow: 0 3px 10px -5px rgb(0 0 0);
        position: absolute;
        top: 19px;
        left: -21px;
    }

        .ribbon p::before {
            content: "";
            position: absolute;
            left: 0px;
            top: 100%;
            z-index: -1;
            border-left: 3px solid #8F0808;
            border-right: 3px solid transparent;
            border-bottom: 3px solid transparent;
            border-top: 3px solid #8F0808;
        }

        .ribbon p::after {
            content: "";
            position: absolute;
            right: 0px;
            top: 100%;
            z-index: -1;
            border-left: 3px solid transparent;
            border-right: 3px solid #8F0808;
            border-bottom: 3px solid transparent;
            border-top: 3px solid #8F0808;
        }

        .bg-home {
            background-image: url('../Advance_CSS/Images/airplane_s7.jpg'); 
            background-repeat: no-repeat; 
            background-size: cover; 
            background-position: 0px 0px;
        }

        @media(max-width:500px) {
            .bg-home {
            background-image: none !important; 
            background-repeat: no-repeat; 
            background-size: cover; 
            background-position: 0px -304px;
        }
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.minus1').click(function () {
                var $input = $(this).parent().find('input');
                var $inputid = $input.attr('id');
                var count = parseInt($input.val()) - 1;
                if ($inputid != "Adult") {
                    count = count <= 0 ? 0 : count;
                }
                else {
                    count = count < 1 ? 1 : count;
                }
                $input.val(count);
                $input.change();
                AddAllPax1();
                return false;
            });
            $('.plus1').click(function () {
                var $input = $(this).parent().find('input');
                let inpcount = parseInt($input.val()) + 1;
                $input.val(inpcount);
                $input.change();
                AddAllPax1();
                return false;
            });

            AddAllPax1();

            function AddAllPax1() {
                let adultinp = $("#AdultF1").val();
                let childinp = $("#ChildF1").val();
                let infantinp = $("#InfantF1").val();
                $("#sapnTotPaxF1").val(parseInt(adultinp) + parseInt(childinp) + parseInt(infantinp) + " Traveler(s)");
            }
        });
    </script>

    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #fff !important">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="slideshow-container">
                        <%=NotificationContent %>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel1" aria-hidden="true" id="mi-modal" style="z-index: 99990 !important;">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 30%;">
            <div class="modal-content">
                <div class="modal-body">
                    <h5>Are you sure you want to continue with this flight?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="modal-btn-cancel" style="float: left; background: #ff0000">Cancel</button>
                    <button type="button" class="btn btn-success" id="modal-btn-confirm">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel2" aria-hidden="true" id="flt-details" style="z-index: 9999 !important;">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="tabs">
                        <div value="1api6ENRMLITZNRML" class="fare-groups-wrap 1api6ENRMLITZNRML_faredetailmasterall " id="1api6ENRMLITZNRML_faredetailmaster" style="margin-bottom: 40px;">
                            <div class="gridViewToolTip1 lft" title="1api6ENRMLITZNRML_O"></div>
                            <ul class="fare-groups nav navbar-nav" role="tablist" style="border-bottom: 1px solid #CCC;">
                                <li class="fgf sel nav-item active" id="1api6ENRMLITZNRML_Allll"><a href="#1api6ENRMLITZNRML_Fare" data-toggle="tab" role="tab" class="div_cls d collapsible gridViewToolTip nav-link active" style="padding: 0px;" id="fare-summ">Fare Details</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_fltdt" data-toggle="tab" role="tab" class="div_cls d collapsible fltDetailslink nav-link" id="fare_Det" style="padding: 0px;">Flight Details</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_bag" data-toggle="tab" role="tab" class="div_cls d collapsible fltBagDetails nav-link" id="bag_det" style="padding: 0px;">Baggage</a>
                                </li>
                                <li class="fgf nav-item"><a href="#1api6ENRMLITZNRML_canc" data-toggle="tab" role="tab" class="div_cls d collapsible fareRuleToolTip cursorpointer nav-link" style="padding: 0px;" id="can_flt">Cancellation</a><div class="fade" title="1api6ENRMLITZNRML_O"></div>

                                </li>
                                <div class="ui_block clearfix"></div>
                            </ul>
                        </div>
                        <div class="tabs-stage tab-content">
                            <div class="depcity tab-pane active" role="tabpanel" id="1api6ENRMLITZNRML_Fare" style="margin-top: -11px;">
                                <div><span onclick="Close('1api6ENRMLITZNRML_');" title="Click to close Details"></span></div>
                                <div class="new-fare-details " id="FareBreak">
                                    <img src="Images/loading_bar.gif" />
                                </div>
                                <div class="clear"></div>
                                <div class="clear"></div>
                            </div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_fltdt" style="margin-top: -11px;"></div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_bag" style="margin-top: -11px;"></div>
                            <div class="depcity tab-pane" role="tabpanel" id="1api6ENRMLITZNRML_canc" style="margin-top: -11px;"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" aria-hidden="true" id="pax-mod">
        <div class="modal-dialog modal-sm" style="margin-top: 10%; width: 50%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row" data-gutter="none">
                        <div id="fltpx">
                        </div>
                        <div class="col-md-12" id="collapseExample4">
                            <input class="aumd-tb div1" id="sapnTotPaxF1" placeholder=" Traveller" type="text" disabled="disabled" />
                            <div id="div_Adult_Child_Infant1hj" class="myText">
                                <div class="innr_pnl dflex">
                                    <div class="main_dv pax-limit" style="margin-left: 70px;">
                                        <label>
                                            <span>Adult</span>
                                        </label>
                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="1" min="1" name="Adult" id="AdultF1">
                                            <span class="plus1">+</span>
                                        </div>
                                    </div>
                                    <div class="main_dv pax-limit" style="margin-left: 70px;">
                                        <label>
                                            <span>Child<span class="light-grey">(2+ 12 yrs)</span></span>
                                        </label>
                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Child" id="ChildF1">
                                            <span class="plus1">+</span>
                                        </div>
                                    </div>
                                    <div class="main_dv pax-limit" style="margin-left: 70px;">
                                        <label>
                                            <span>Infant <span class="light-grey">(below 2 yrs)</span></span>
                                        </label>
                                        <div class="number">
                                            <span class="minus1">-</span>
                                            <input type="text" class="inp" value="0" min="0" name="Infant" id="InfantF1">
                                            <span class="plus1 Infant1">+</span>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </div>



                    </div>

                </div>
                <div class="modal-footer hidden">
                    <button type="button" class="btn btn-success" id="pax-confirm">Confirm</button>

                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="" id="pass-pax" />

    <div class="theme-hero-area theme-hero-area-primary">

        <div class="theme-hero-area-body">
            <div class=" _pb-200 _pv-mob-50 bg-home">
                <div class="container">
                    <div class="theme-search-area-tabs vertical_search_engine">
                        <div class="tab-content">
                            <div>
                                <h4 style="font-size: 35px; color: #fff; font-weight: 400;">Book Your Flight</h4>
                            </div>
                            <div class="tab-pane fade in active" id="tab1default1">
                                <Search:IBESearch ID="IBESearch2" runat="server" />
                                <SearchDep:IBESearchDep runat="server" ID="FixDep" />
                            </div>
                            <div class="tab-pane fade" id="tab2default">
                                <Searchsss:BusSearch ID="Bus2" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="theme-page-section" style="background: #06060680;margin-top: 58px;padding: 40px;border-radius: 10px;">
                        <div class="container">
                            <div class="row row-col-mob-gap" data-gutter="60">
                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-airplane"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Flight</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-airplane-alt"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">FDD</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-building"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Hotels</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-train-line"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Trains</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-bus-alt-3"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Bus</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-beach"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Holidays</h5>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-bank-transfer"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">DMT</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-box"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Cargo</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-ui-love-add"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Insurance</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-id"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">AEPS</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-1 col-xs-4">
                                    <div class="feature feature-white feature-center">
                                        <div class="single-choose" style="margin-bottom: 12px;">
                                            <i class="icofont-stock-mobile"></i>
                                        </div>
                                        <div class="feature-caption">
                                            <h5 class="feature-title">Utility</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="features">

    <div class="container">
        <div class="right_data">
            <FDCAL:FareCal ID="FDCAL1" runat="server" />
        </div>
    </div>
        </section>

   
    <div class="theme-hero-area" id="div_LatestOffers" style="display:none;">
        <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg-pattern theme-hero-area-bg-pattern-ultra-light" style="background-image: url(../Advance_CSS/img/patterns/travel/2.png);"></div>
            <div class="theme-hero-area-grad-mask" style="background: #4c4c4c;"></div>
            <div class="theme-hero-area-inner-shadow theme-hero-area-inner-shadow-light"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="theme-page-section-xxl">
         <div class="container">
              <div class="theme-page-section-header">
          <h5 class="theme-page-section-title" style="color: #c7c7c7;">Series Flights Avilable</h5>
          <p class="theme-page-section-subtitle" style="color: #eee;">The most searched countries in March</p>
        </div>
                           
                                <div class="theme-page-section-header _ta-l" style="display:none;">
          <h5 class="theme-page-section-title">Our New FDD Flights Available</h5>
          <p class="theme-page-section-subtitle"><a>Filter Sectors</a></p>
          <a class="theme-page-section-header-link theme-page-section-header-link-rb" href="#">Filter Option</a>
                                    <div class="col-md-4"> 
                                        <asp:DropDownList ID="ddlFromSector" runat="server" class="selectpicker" data-show-subtext="true" data-live-search="true">
                                            <asp:ListItem>Select From Sector</asp:ListItem>
                                           
                                        </asp:DropDownList>
             
                                        </div>

                                     <div class="col-md-4"> 
                                        <asp:DropDownList ID="ddLToSector" runat="server" class="selectpicker" data-show-subtext="true" data-live-search="true">
                                            <asp:ListItem>Select From Sector</asp:ListItem>
                                           
                                        </asp:DropDownList>
             
                                        </div>
        </div>
                                <div id="avlsector_id" style="display: none;">
                                  <%--  <b class='status_info fl'>
                                        <i class='icofont-location-pin icofont-2x'></i></b>
                                    <span class='status_cont'>Avilable Sector</span>
                                     <button id="slideFront" type="button" style="float:right;border:none;background:none;"><i class="icofont-dotted-right icofont-2x"></i></button>
                                    <button id="slideBack" type="button" style="float:right;border:none;background:none;"><i class="icofont-dotted-left icofont-2x"></i></button>--%>
                                   
                                </div>

                                <div class="tab">
                                    <div id="container" style="display:none;">
                                        <style>
                                            div#field-dropdown-autocomplete {
                                                width: 30%;
                                                display: none;
                                            }
                                        </style>
                                        <select name="field-dropdown"  id="field-dropdown" style="display:none;">
                                            <option value="">Select</option>
                                        </select>

                                    </div>
                                    <div id="BindDepArrDetails">
                                        <p class="text-center" style="font-size: 20px;color:#fff;">Please wait... <i class="fa fa-spinner fa-pulse"></i></p>
                                    </div>
                                </div>
                            
             </div>
                   </div>
            </div>
        </div>
      


                        <%--<script type="text/javascript">
                            var button = document.getElementById('slideFront');
                            button.onclick = function () {
                                var container = document.getElementById('container');
                                sideScroll(container, 'right', 25, 100, 10);
                            };

                            var back = document.getElementById('slideBack');
                            back.onclick = function () {
                                var container = document.getElementById('container');
                                sideScroll(container, 'left', 25, 100, 10);
                            };

                            function sideScroll(element, direction, speed, distance, step) {
                                scrollAmount = 0;
                                var slideTimer = setInterval(function () {
                                    if (direction == 'left') {
                                        element.scrollLeft -= step;
                                    } else {
                                        element.scrollLeft += step;
                                    }
                                    scrollAmount += step;
                                    if (scrollAmount >= distance) {
                                        window.clearInterval(slideTimer);
                                    }
                                }, speed);
                            }

                        </script>--%>

                        <div class="theme-hero-area" id="expired-offer" style="display:none;">
                            <div class="theme-hero-area-bg-wrap">
            <div class="theme-hero-area-bg-pattern theme-hero-area-bg-pattern-ultra-light" style="background-image: url(../Advance_CSS/img/patterns/travel/2.png);"></div>
            <div class="theme-hero-area-grad-mask" style="background: #4c4c4c;"></div>
            <div class="theme-hero-area-inner-shadow theme-hero-area-inner-shadow-light"></div>
        </div>
        <div class="theme-hero-area-body">
            <div class="theme-page-section-xxl">
                            <div class="container">
                            <div class="theme-page-section-header">
          <h5 class="theme-page-section-title" style="color: #c7c7c7;">Limited Offer</h5>
          <p class="theme-page-section-subtitle" style="color: #eee;">Book your flights before it expire.</p>
        </div>
                            <div class="row resources ">
            <div class="container" id="resource-slider">
                             <span class="arrow prev"></span>
                <span class="arrow next"></span>
                              <div class="resource-slider-frame">
                                <div class="slide divLimitedOfferExpireSoon" id="slide">
                                    <p class="text-center" style="font-size: 15px; margin: 75px 10px 10px 300px;">Please wait... <i class="fa fa-spinner fa-pulse"></i></p>
                                </div>
                            </div>
                          </div>
                                </div>
                                </div>
                </div>
            </div>
                            
                        </div>

            



    <section id="features" class="features">
        <div class="container">

            <div class="section-title">
                <h2>Quick Links</h2>
            </div>

            <div class="col-md-12">
                <div class="content">
                    <div class="row">

                        <div class="col-md-4">
                            <a href="<%=LastSearching %>">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-airplane box"></i>

                                </div>
                                <div class="--text">Last Searching</div>
                            </div>
                                </a>
                        </div>

                        <div class="col-md-4">
                            <a href="<%=LastSearching %>">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-paper-plane box"></i>

                                </div>
                                <div class="--text">Last Booking</div>
                            </div>
                                </a>
                        </div>

                        <div class="col-md-4">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-airplane-alt box"></i>

                                </div>
                                <div class="--text">Travel Calander</div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <a href="<%=BankDetail %>">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-bank-alt box"></i>

                                </div>
                                <div class="--text">Bank Details</div>
                            </div>
                                </a>
                        </div>

                        <div class="col-md-4">
                            <a href="<%=LedgerDetail %>">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-notebook box"></i>

                                </div>
                                <div class="--text">Ledger Details</div>
                            </div>
                                </a>
                        </div>

                        <div class="col-md-4">
                            <a href="<%=UploadMoney %>">
                            <div class="btn-track">
                                <div class="--icon">
                                    <div class="circle-inner"></div>
                                    <div class="circle-outer"></div>
                                    <i class="icofont-money-bag box"></i>

                                </div>
                                <div class="--text">Upload Money</div>
                            </div>
                                </a>
                        </div>



                    </div>
                </div>
            </div>



        </div>
    </section>

<%--    <script type="text/javascript" src="Advance_CSS/dropdown_search_box/js/select2.min.js"></script>--%>

   <%-- <script type="text/javascript">
        "use strict";

        productScroll();

        function productScroll() {
            let slider = document.getElementById("slider");
            let next = document.getElementsByClassName("pro-next");
            let prev = document.getElementsByClassName("pro-prev");
            let slide = document.getElementById("slide");
            let item = document.getElementById("slide");

            for (let i = 0; i < next.length; i++) {
                //refer elements by class name

                let position = 0; //slider postion

                prev[i].addEventListener("click", function () {
                    //click previos button
                    if (position > 0) {
                        //avoid slide left beyond the first item
                        position -= 1;
                        translateX(position); //translate items
                    }
                });

                next[i].addEventListener("click", function () {
                    if (position >= 0 && position < hiddenItems()) {
                        //avoid slide right beyond the last item
                        position += 1;
                        translateX(position); //translate items
                    }
                });
            }

            function hiddenItems() {
                //get hidden items
                let items = getCount(item, false);
                let visibleItems = slider.offsetWidth / 210;
                return items - Math.ceil(visibleItems);
            }
        }

        function translateX(position) {
            //translate items
            slide.style.left = position * -210 + "px";
        }

        function getCount(parent, getChildrensChildren) {
            //count no of items
            let relevantChildren = 0;
            let children = parent.childNodes.length;
            for (let i = 0; i < children; i++) {
                if (parent.childNodes[i].nodeType != 3) {
                    if (getChildrensChildren)
                        relevantChildren += getCount(parent.childNodes[i], true);
                    relevantChildren++;
                }
            }
            return relevantChildren;
        }

    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (localStorage.getItem('isnotification') != 'notyshown') {
                $('#exampleModal').modal('show');
                localStorage.setItem('isnotification', 'notyshown');
            }
        });
    </script>


    <script type="text/javascript">
        $('.toggle-tickets').click(function () {
            $tickets = $(this).parent().siblings('.collapse');

            if ($tickets.hasClass('in')) {
                $tickets.collapse('hide');
                $(this).html('Show Tickets');
                $(this).closest('.ticket-card').removeClass('active');
            } else {
                $tickets.collapse('show');
                $(this).html('Hide Tickets');
                $(this).closest('.ticket-card').addClass('active');
            }
        });
    </script>--%>
    
</asp:Content>
