﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterForHome.master" AutoEventWireup="false" CodeFile="utilityandbill.aspx.vb" Inherits="Utility_utilityandbill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style type="text/css">
        .bg-secondary {
    background-color: #0c2f55 !important;
}
.pb-5, .py-5 {
    padding-bottom: 3rem !important;
}
.pt-4, .py-4 {
    padding-top: 1.5rem !important;
}

.secondary-nav.nav.alternate {
    margin-bottom: 10px;
}
.secondary-nav.nav {
    padding-top: 12px;
    padding-bottom: 0px;
    padding-left: 8px;
}

.secondary-nav.nav.alternate .nav-item .nav-link.active {
    background-color: transparent;
    color: #fff;
    border-bottom: 3px
 solid #0071cc;
}
.secondary-nav.nav .nav-item .nav-link.active {
    background: #fff;
    color: #0071cc;
    border-radius: 4px 4px 0px 0px;
}
.secondary-nav.nav .nav-item:first-child .nav-link {
    margin-left: 0px;
}
.secondary-nav.nav.alternate .nav-link {
    padding: 0.3rem 15px;
}
.secondary-nav.nav .nav-link {
    text-align: center;
    font-size: 13px;
    font-size: 0.8125rem;
    margin: 0 10px;
    padding: 0.6rem 15px;
    color: #8298af;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
}

.shadow-md {
    -webkit-box-shadow: 0px 0px 50px -35px rgb(0 0 0 / 40%);
    box-shadow: 0px 0px 50px -35px rgb(0 0 0 / 40%);
}

.text-4 {
    font-size: 18px !important;
    font-size: 1.125rem !important;
}

.form-row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -5px;
    margin-left: -5px;
}

.rounded {
    border-radius: 0.25rem !important;
}
.bg-white {
    background-color: #fff!important;
}

.view-plans-link {
    position: absolute;
    right: 0px;
    z-index: 4;
    padding: 0 15px;
    font-size: 13px;
    text-decoration: underline;
    -ms-flex-align: center;
    align-items: center;
    display: -ms-flexbox;
    display: flex;
    height: 100%;
}
    </style>


    <div class="bg-secondary pt-4 pb-5">
      <div class="container">
      <!-- Secondary Navigation
      ============================================= -->
      <ul class="nav secondary-nav alternate">
          <li class="nav-item"> <a class="nav-link active" href="index-2.html"><span><i class="fas fa-mobile-alt"></i></span> Mobile</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-dth-2.html"><span><i class="fas fa-tv"></i></span> DTH</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-datacard-2.html"><span><i class="fas fa-credit-card"></i></span> DataCard</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-broadband-2.html"><span><i class="fas fa-wifi"></i></span> Broadband</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-landline-2.html"><span><i class="fas fa-phone"></i></span> Landline</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-cabletv-2.html"><span><i class="fas fa-plug"></i></span> CableTv</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-electricity-2.html"><span><i class="fas fa-lightbulb"></i></span> Electricity</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-metro-2.html"><span><i class="fas fa-subway"></i></span> Metro</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-gas-2.html"><span><i class="fas fa-flask"></i></span> Gas</a> </li>
          <li class="nav-item"> <a class="nav-link" href="recharge-bill-water-2.html"><span><i class="fas fa-tint"></i></span> Water</a> </li>
        </ul><!-- Secondary Navigation end -->
        
      <!-- Mobile Recharge
      ============================================= -->
      <div class="bg-white shadow-md rounded px-4 pt-4 pb-3">
            <h2 class="text-4 mb-3">Mobile Recharge or Bill Payment</h2>
            <form id="recharge-bill" method="post">
              <div class="mb-3">
                <div class="custom-control custom-radio custom-control-inline">
                  <input id="prepaid" name="rechargeBillpayment" class="custom-control-input" checked="" required="" type="radio">
                  <label class="custom-control-label" for="prepaid">Prepaid</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input id="postpaid" name="rechargeBillpayment" class="custom-control-input" required="" type="radio">
                  <label class="custom-control-label" for="postpaid">Postpaid</label>
                </div>
              </div>
              <div class="form-row">
              <div class="col-md-6 col-lg-3 form-group">
                <input type="text" class="form-control" data-bv-field="number" id="mobileNumber" required="" placeholder="Enter Mobile Number">
              </div>
              <div class="col-md-6 col-lg-3 form-group">
                <select class="custom-select" id="operator" required="">
                  <option value="">Select Your Operator</option>
                  <option>1st Operator</option>
                  <option>2nd Operator</option>
                  <option>3rd Operator</option>
                  <option>4th Operator</option>
                  <option>5th Operator</option>
                  <option>6th Operator</option>
                  <option>7th Operator</option>
                </select>
              </div>
              <div class="col-md-6 col-lg-3 form-group">
                <a href="#" data-target="#view-plans" data-toggle="modal" class="view-plans-link">View Plans</a>
                <input class="form-control" id="amount" placeholder="Enter Amount" required="" type="text">
              </div>
              <div class="col-md-6 col-lg-3 form-group">
              <a class="btn btn-primary btn-block" href="recharge-order-summary.html">Continue</a>
              </div>
              </div>
            </form>
      </div><!-- Mobile Recharge end -->
    </div>
    </div>

</asp:Content>

